from tkinter import Tk , Canvas , Scrollbar , Label , Frame  , Text , END 
from tkinter.filedialog import askopenfile , asksaveasfile 
from tkinter.messagebox import showwarning 
from tkinter.font import families 

root=Tk()
root.geometry('600x600')
root.title('Text Editor')

fontsize=12
dark=True
light=False
file=None 
currentfont='Fira Code Nerd Font Mono'

text=Text(root , font=('Fira Code Nerd Font Mono' , fontsize) , bg="#282828" , fg="#d4be98" , insertbackground="#d4be98")
text.pack(side="left" , expand=True , fill="both")

sbar=Scrollbar(root , orient="vertical")
sbar.pack(side="right" , fill="y")

def theme_toggle():
    global dark , light
    if dark==True:
       light=True;dark=False 
       text.configure(bg="#f9f5d7" , fg="#654735" , insertbackground="#654735")
    elif light==True:
       dark=True;light=False
       text.configure(bg="#282828" , fg="#d4be98" , insertbackground="#d4be98")

class FontSize():
    def larger_size():
       global fontsize
       fontsize=fontsize+1 
       text.configure(font=('Fira Code Nerd Font Mono' , fontsize))

    def smaller_size():
       global fontsize
       fontsize=fontsize-1
       text.configure(font=('Fira Code Nerd Font Mono' , fontsize))

class FileOperations():
    def open_f():
       global file 
       file=askopenfile("r+")
       if file!=None:
         try:
           contents=file.read()
           text.delete(1.0 , END)
           text.insert(END , contents)
         except UnicodeDecodeError:
           file=None 
           showwarning('Warning' , 'The file you tried to open is not supported..')
       else:
         pass 

    def saveas_f():
       filetosave=asksaveasfile()
       if not filetosave==None:
          contents=text.get(1.0 , END)
          filetosave.write(contents)
       else:
         pass

    def save_f():
       if file==None:
          FileOperations.saveas_f()
       else:
          file.seek(0)
          contents=text.get(1.0 , END)
          file.write(contents) 
          file.truncate()

text.config(yscrollcommand=sbar.set)
sbar.config(command=text.yview)

Custom_Fonts.main_db()

root.bind('<Control-o>' , lambda event:FileOperations.open_f())
root.bind('<Control-s>' , lambda event:FileOperations.save_f())
root.bind('<Control-Shift-S>' , lambda event:FileOperations.saveas_f())
root.bind('<Control-Shift-C>' , lambda event:theme_toggle())
root.bind('<Control-Shift-plus>' , lambda event:FontSize.larger_size())
root.bind('<Control-minus>' , lambda event:FontSize.smaller_size())
root.mainloop()

