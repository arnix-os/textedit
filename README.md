# textedit

A fast and minimal keychords based text editor

# Keychords

- Ctrl + S = Save
- Ctrl + O = Open
- Ctrl + Shift + S = Save As
- Ctrl + Shift + C = Toggle Light/Dark Theme
- Ctrl + Shift + F = Font Switching Dialog
- Ctrl + minus = Decrease font size
- Ctrl + Shift + Plus = Increase font size
